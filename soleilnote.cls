% SOLEIL Upgrade document template v0.1
% License: CC BY 4.0

% Adapted from CERN ATLAS Note document Class version 0.1
% Adapted for use in ATLAS notes by Thorsten Wengler from:
% CERN Accelerator and Technology Sector Note document class Version 0.1

% For the original version and a detailed historic of the file:
% https://www.overleaf.com/latex/templates/cern-atlas-note-template/bstvyvnjqgcf 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
\def\fileversion{0.1}
\def\filedate{21/07/2020}
\def\docdate {21/07/2020}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{soleilnote}%
% [\filedate\space version \fileversion]

% Conditions in LaTeX for the public/internal/confidential document type
% Ref: https://tex.stackexchange.com/questions/282010/loading-packages-in-custom-class-based-on-option
\RequirePackage{ifthen}
\newboolean{public}
\newboolean{internal}
\newboolean{confidential}

\newboolean{tocraggedright}
\setboolean{tocraggedright}{true}

\newboolean{upgrade}
% By default, document is not an upgrade document
\setboolean{upgrade}{false}

\DeclareOption{public}{%
    % Public document: green table header, P abbreviation
    \setboolean{public}{true}}
    
\DeclareOption{internal}{%
    % Internal document: red table header, I abbreviation
    \setboolean{internal}{true}}

\DeclareOption{confidential}{%
    % Confidential document: red table header, C abbreviation
    \setboolean{confidential}{true}}
    
\DeclareOption{upgrade}{%
    % Upgrade document: specific logo and WP number
    \setboolean{upgrade}{true}}

\DeclareOption{tocjustified}{%
    % Putting all numbers in the TOC on the right, so it is easier to sum them them up.
    \setboolean{tocraggedright}{false}}

\ProcessOptions

\LoadClass[12pt]{article}
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
% Load the Linux Libertine font family, and the associated math font
% For more font options see
% https://tex.stackexchange.com/questions/59702/suggest-a-nice-font-family-for-my-basic-latex-template-text-and-math
% \RequirePackage{libertine,libertinust1math}

% Maths package
\RequirePackage{amsmath, amsfonts, amssymb}

% Change the monospace font 
% https://tug.org/FontCatalogue/inconsolata/
% \RequirePackage{inconsolata}
\RequirePackage[scaled=.85]{FiraMono}

% SOLEIL notes are typeset with a sans serif font.
% Fira fonts are used for the main document. We use here newtxsf as a math font since unicode-math from is not supported with pdfLaTeX (theerefore firasansmath can only be used with Xe/Lua LaTeX)
\RequirePackage[sfdefault,scaled=.85]{FiraSans}
\RequirePackage[scaled=0.90]{newtxsf}


% Modify the page geometry (margins)
\RequirePackage[a4paper,
left=31mm, right=31mm, top=35mm, bottom=35mm,
headheight=15pt, marginparwidth=26mm]{geometry}


% Some utilities for tables
\RequirePackage{array}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{longtable}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
% \RequirePackage{ltablex}
\RequirePackage{booktabs}



% Booktabs introduces some top and bottom space in the tables to make them nicer
% These spaces will have to be filled
% https://tex.stackexchange.com/questions/266438/booktabs-and-colortbl-not-filling-height-of-row
\newcommand*{\belowrulesepcolor}[1]{%
  \noalign{%
    \kern-\belowrulesep
    \begingroup
      \color{#1}%
      \hrule height\belowrulesep
    \endgroup
  }%
}
\newcommand*{\aboverulesepcolor}[1]{%
  \noalign{%
    \begingroup
      \color{#1}%
      \hrule height\aboverulesep
    \endgroup
    \kern-\aboverulesep
  }%
}

% Color package, and SOLEIL specific colors definition
\RequirePackage[dvipsnames*,svgnames, table]{xcolor}
\definecolor{rougeSOLEIL}{RGB}{255, 0, 0}
\definecolor{orangeSOLEIL}{RGB}{255, 192, 0}
\definecolor{vertSOLEIL}{RGB}{0, 176, 80}
\definecolor{rougeWPSOLEIL}{RGB}{231, 58, 57}
\definecolor{orangeWPSOLEIL}{RGB}{243, 150, 57}
\definecolor{vertWPSOLEIL}{RGB}{75, 173, 55}
\definecolor{bleuclairWPSOLEIL}{RGB}{76, 194, 241}
\definecolor{bleufonceWPSOLEIL}{RGB}{32, 60, 137}
\definecolor{violetWPSOLEIL}{RGB}{134, 81, 155}

% Graphics inclusions and TikZ/PGF
\RequirePackage{graphicx}
\RequirePackage{tikz}


% Switch colors according to document type
\ifthenelse{\boolean{public}}{%
    \newcommand{\documentTableColor}{vertSOLEIL!60}
    \newcommand{\documentAbbreviation}{{\sffamily\textcolor{vertSOLEIL}{P}}}
    \newcommand{\documentFullType}{{\sffamily\textcolor{vertSOLEIL}{Public}}}
}{}

\ifthenelse{\boolean{internal}}{%
    \newcommand{\documentTableColor}{orangeSOLEIL!60}
    \newcommand{\documentAbbreviation}{{\sffamily\textcolor{orangeSOLEIL}{I}}}
    \newcommand{\documentFullType}{{\sffamily\textcolor{orangeSOLEIL}{Interne}}}
}{}

\ifthenelse{\boolean{confidential}}{%
    \newcommand{\documentTableColor}{rougeSOLEIL!60}
    \newcommand{\documentAbbreviation}{{\sffamily\textcolor{rougeSOLEIL}{C}}}
    \newcommand{\documentFullType}{{\sffamily\textcolor{rougeSOLEIL}{Confidentiel}}}
}{}

% Switch the logo and remove Workpackage reference in case of an upgrade document 
\ifthenelse{\boolean{upgrade}}{%
    \newcommand{\documentLogoPath}{images/template/logo_SOLEIL_upgrade.png}
    \newcommand{\documentWPName}{\sffamily\bfseries\LARGE \@WorkPackageNumber}
}{%
    \newcommand{\documentLogoPath}{images/template/logo_SOLEIL.png}
    \newcommand{\documentWPName}{~}}
    

% Date formatting
\RequirePackage{datetime2}

% Section titles formatting
\RequirePackage{sectsty}
\allsectionsfont{\normalfont\sffamily\bfseries}

% To obtain better looking Table of contents
\RequirePackage{tocloft}

\renewcommand{\cfttoctitlefont}{\LARGE\sffamily}
\renewcommand{\cftloftitlefont}{\LARGE\sffamily}
\renewcommand{\cftlottitlefont}{\LARGE\sffamily}

% Update the TOC design: for each type of entry, the first two commands would put the page number next to the entry text. Third command puts the entry in sans serif font

\ifthenelse{\boolean{tocraggedright}}{%
% Update \parts in ToC
\renewcommand{\cftpartleader}{~}% Content between part title and page number
\renewcommand{\cftpartafterpnum}{\hfill\mbox{}}% Content after part page number

% Update \sections in ToC
\renewcommand{\cftsecleader}{~}% Content between section title and page number
\renewcommand{\cftsecafterpnum}{\hfill\mbox{}}% Content after section page number

% Update \subsections in ToC
\renewcommand{\cftsubsecleader}{~}% Content between subsection title and page number
\renewcommand{\cftsubsecafterpnum}{\hfill\mbox{}}% Content after subsection page number

% Update \subsubsections in ToC
\renewcommand{\cftsubsubsecleader}{~}% Content between subsection title and page number
\renewcommand{\cftsubsubsecafterpnum}{\hfill\mbox{}}% Content after subsection page number

% Update \figure in ToC
\renewcommand{\cftfigleader}{~}% Content between subsection title and page number
\renewcommand{\cftfigafterpnum}{\hfill\mbox{}}% Content after subsection page number

% Update \subsfigure in ToC
% \renewcommand{\cftsubfigafterpnum}{\hfill\mbox{}}% Content after subsection page number
% \renewcommand{\cftsubfigfont}{\sffamily}

% Update \table in ToC
\renewcommand{\cfttableader}{~}% Content between subsection title and page number
\renewcommand{\cfttabafterpnum}{\hfill\mbox{}}% Content after subsection page number

% Update \subtable in ToC
% \renewcommand{\cftsubtabafterpnum}{\hfill\mbox{}}% Content after subsection page number
% \renewcommand{\cftsubtabfont}{\sffamily}
}{}

% Update \parts in ToC
\renewcommand{\cftpartfont}{\sffamily}
% Update \sections in ToC
% \renewcommand{\cftsecleader}{~}% Content between section title and page number
% \renewcommand{\cftsecafterpnum}{\hfill\mbox{}}% Content after section page number
\renewcommand{\cftsecfont}{\sffamily}
% Update \subsections in ToC
% \renewcommand{\cftsubsecleader}{~}% Content between subsection title and page number
% \renewcommand{\cftsubsecafterpnum}{\hfill\mbox{}}% Content after subsection page number
\renewcommand{\cftsubsecfont}{\sffamily}
% Update \subsubsections in ToC
% \renewcommand{\cftsubsubsecleader}{~}% Content between subsection title and page number
% \renewcommand{\cftsubsubsecafterpnum}{\hfill\mbox{}}% Content after subsection page number
\renewcommand{\cftsubsubsecfont}{\sffamily}
% Update \figure in ToC
% \renewcommand{\cftfigleader}{~}% Content between subsection title and page number
% \renewcommand{\cftfigafterpnum}{\hfill\mbox{}}% Content after subsection page number
\renewcommand{\cftfigfont}{\sffamily}
% Update \subsfigure in ToC
% \renewcommand{\cftsubfigleader}{~}% Content between subsection title and page number
% \renewcommand{\cftsubfigafterpnum}{\hfill\mbox{}}% Content after subsection page number
% \renewcommand{\cftsubfigfont}{\sffamily}
% Update \table in ToC
% \renewcommand{\cfttableader}{~}% Content between subsection title and page number
% \renewcommand{\cfttabafterpnum}{\hfill\mbox{}}% Content after subsection page number
\renewcommand{\cfttabfont}{\sffamily}
% Update \subtable in ToC
% \renewcommand{\cftsubtableader}{~}% Content between subsection title and page number
% \renewcommand{\cftsubtabafterpnum}{\hfill\mbox{}}% Content after subsection page number
% \renewcommand{\cftsubtabfont}{\sffamily}

% Remove the dots entries in the TOC
\renewcommand{\cftdot}{}

% Provide customized header and footer
% We define a special footer for the first page only
\RequirePackage{fancyhdr}
\fancypagestyle{specialfooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[R]{{\footnotesize\sffamily\textbf{Synchrotron SOLEIL} – Société Civile au capital de 12.000\,€\\
                    439 684 903 R.C.S. EVRY – NAF 7219Z – SIRET 439 684 903 00016\\
                    L’Orme des Merisiers – Saint-Aubin – BP 48 – 91192 Gif-sur-Yvette Cedex\\[-0.25em]
                    \url{www.synchrotron-soleil.fr}}}
}

% Provide the total page number for the footer
\RequirePackage{lastpage}

% Uncomment to remove the header rule
\renewcommand{\headrulewidth}{0pt} 
\pagestyle{fancy}
\fancyhead{}
\fancyhead[L]{\sffamily Synchrotron SOLEIL}
\fancyhead[C]{}
\fancyhead[R]{\sffamily \@DocReferenceNumber \quad \textbf{\documentAbbreviation}}
\fancyfoot[L]{\begin{minipage}[b]{0.5\textwidth}\sffamily \@title \end{minipage}\\{\footnotesize La version électronique fait foi.}}
\fancyfoot[C]{}
\fancyfoot[R]{\sffamily Page \thepage\,/\,\pageref*{LastPage}}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}



% The maketile, where the magic happens
% Add the SOLEIL logo on top left, the document reference on top right
% (aligned with the logo bottom)
% Below add the document type abbreviation P/I/C
% Add the work package and the title
\renewcommand\maketitle{%
    \pagestyle{empty}
    \newgeometry{top=20mm,bottom=30mm,right=20mm,left=20mm}
    \thispagestyle{specialfooter}
    \fancyfootoffset{0pt}% <- recalculate \headwidth
    { \setlength{\parindent}{0pt}
    \begin{tabular}[t]{@{}p{0.30\textwidth}}
        \protect{\includegraphics[width=35mm]{\documentLogoPath}} 
    \end{tabular}
    \hfill
    \begin{tabular}[b]{r@{}}
         {\sffamily\normalsize Référence : \@DocReferenceNumber}
    \end{tabular}
    \par
    \vskip2em
    \hfill
    \begin{tabular}[b]{r@{}}
         {\fontsize{50}{60}\selectfont \documentAbbreviation}
    \end{tabular}
    \par
    \vskip5em
    {\documentWPName}
    \vskip\baselineskip
    {\sffamily\bfseries\LARGE\raggedright\hyphenpenalty=10000\@title\par}
    \vskip4em
    {\fontsize{35}{42}\textsc{\documentFullType}}
    \vskip\baselineskip
    {\sffamily La version électronique fait foi.}
    \vskip5ex
    \begin{tikzpicture}[remember picture,overlay]
    \node[anchor=south west,inner sep=0pt] at (current page.south west)
              {\includegraphics[width=\paperwidth]{images/template/background_title_page.jpg}};
    \end{tikzpicture}
    \RevisionHistory
    % \hfill
    % \par
    }
% \thispagestyle{empty}
  \endtitlepage
  \restoregeometry
  }

% Avoid a page break after the maketitle
% \let\endtitlepage\relax

% Revision table command from https://tex.stackexchange.com/questions/403361/creating-a-command-to-fill-a-template-table
% We must also tweak booktabs so that the color fills the first row
% https://tex.stackexchange.com/questions/11198/coloring-columns-in-a-table-with-colortbl-and-booktabs
% https://tex.stackexchange.com/questions/177202/booktabs-and-row-color
\RequirePackage{expl3}

\newcommand{\headcol}{\rowcolor{\documentTableColor}}
% \newcommand{\topline}{\arrayrulecolor{black}\specialrule{0.1em}{\abovetopsep}{0.5pt}%
            % \arrayrulecolor{\documentTableColor}\specialrule{\belowrulesep}{0pt}{-3pt}%
            % \arrayrulecolor{black}}
\newcommand{\topline}{\arrayrulecolor{black}\specialrule{0.1em}{\abovetopsep}{0pt}\arrayrulecolor{\documentTableColor}\specialrule{\belowrulesep}{0pt}{0pt}\arrayrulecolor{black}}
\newcommand{\midline}{\arrayrulecolor{\documentTableColor}\specialrule{\aboverulesep}{-1pt}{0pt}%
            \arrayrulecolor{black}\specialrule{\lightrulewidth}{0pt}{0pt}%
            \arrayrulecolor{white}\specialrule{\belowrulesep}{0pt}{-3pt}%
            \arrayrulecolor{black}}

           
\ExplSyntaxOn
\seq_new:N \l_revisions_seq
\newcommand\addrevision[1]{% add to \l_revisions_seq
  \seq_put_right:Nn \l_revisions_seq {#1}
}
\seq_new:N \l_recipients_seq

\newcommand\addrecipients[1]{#1}

\newcommand\RevisionHistory{%
\begin{center}
\small
\begin{longtable}{*{4}{>{\raggedright\arraybackslash}p{0.13\textwidth}} >{\raggedright\arraybackslash}p{0.356\textwidth}}
     \toprule
     \belowrulesepcolor{\documentTableColor}
     \headcol \textbf{Date} & \textbf{Rédacteur} & \textbf{Vérificateur} & \textbf{Approbateur} & \textbf{Modification}\\
     \aboverulesepcolor{\documentTableColor}
     \midrule
    %  \midline
     \endfirsthead
    %  \topline
    \toprule
    \belowrulesepcolor{\documentTableColor}
    \headcol \textbf{Date} & \textbf{Rédacteur} & \textbf{Vérificateur} & \textbf{Approbateur} & \textbf{Modification}\\
     \aboverulesepcolor{\documentTableColor}
     \midrule
    %  \midline
     \endhead
     \bottomrule
     \multicolumn{5}{r}{Suite~sur~la~prochaine~page}
     \endfoot
     \bottomrule
     \endlastfoot
     \seq_map_inline:Nn \l_revisions_seq {% loop over revisions
      \regex_split:nnN {\;} {##1} \l_tmpa_seq% split on commas
      \seq_use:Nn \l_tmpa_seq {&}% print the table row
      \\[0.5em]
     }
     \\[-12pt]% \\ is necessary here but it inserts a blank line, so hack...
     \midrule
     \textbf{Destinataires} & \multicolumn{4}{l}{\@ListOfRecipients}\\
\end{longtable}
\end{center}
}
\ExplSyntaxOff

% \gdef\@keywords{{\tt SOLEIL, Upgrade}}


\def\@DocReferenceNumber{}
\newcommand{\DocReferenceNumber}[1]%
{
  \def\@DocReferenceNumber{#1}
}

\def\@WorkPackageNumber{}
\newcommand{\WorkPackageNumber}[1]%
{
  \def\@WorkPackageNumber{#1}
}

\def\@ListOfRecipients{}
\newcommand{\ListOfRecipients}[1]%
{
  \def\@ListOfRecipients{#1}
}
\endinput
