\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Soleil2Beamer}[2024/04/23 Beamer Presentation Class for SOLEIL II]
%
% Copyright: Patrick Schreiber
% Major Modifications:
%    04/2024 Initial version adapted to new corporate design

\LoadClass[10pt,aspectratio=169]{beamer}
\RequirePackage{tikz}
\usetikzlibrary{calc,shapes.misc}
\RequirePackage{transparent}
\RequirePackage{xcolor}
\RequirePackage{geometry}
\RequirePackage{helvet}
\RequirePackage{changepage}
\RequirePackage{xifthen}
\RequirePackage{setspace}

\makeatletter
\def\@author{}
\def\@mainauthor{}
\renewcommand{\author}[2][]{\def\@author{#2}\def\@mainauthor{#1}}
\renewcommand{\insertauthor}{\@author}
\let\@otitle\title
\renewcommand{\title}[2][16]{
    \def\@titlesize{#1}
    \@otitle{#2}
}
\newcommand{\insertmainauthor}{\@mainauthor}

\def\theevent{}
\newcommand{\event}[1]{\def\theevent{#1}}

\def\@showmetadata{0}
\newcommand{\showauthordate}{\def\@showmetadata{1}}
\makeatother

\definecolor{soleilyellow}{RGB}{255,204,0}
\definecolor{soleilblue}{RGB}{14,65,148}
\definecolor{soleilgrey}{RGB}{64,64,64}

\setbeamerfont{frametitle}{series=\bfseries}

\usebackgroundtemplate{%  Background Template for all slides
  \begin{tikzpicture}[remember picture,overlay]
      \node[xshift=0.79cm,yshift=-0.51cm] at (current page.north west) {\includegraphics[width=1.41cm,trim={0cm 0cm 4.8cm 0cm},clip]{./images/soleil_logo.png}};
      \node[xshift=0.36cm,yshift=0.850cm] at (current page.south west) {\includegraphics[width=0.735cm]{./images/soleil_bottomleft.png}};
      \filldraw[fill=soleilblue,draw=soleilblue] ($ (current page.north west)+ (1.84cm, -0.75cm)$) rectangle ($ (current page.north west)+ (20cm, -0.75cm)$);
  \end{tikzpicture}
}

% Set Colors
\setbeamercolor{titlelike}{parent=structure,fg=soleilblue}
\setbeamercolor{itemize/enumerate body}{fg=soleilblue}
\setbeamercolor{itemize/enumerate subbody}{fg=soleilgrey}
\setbeamercolor{itemize/enumerate subsubbody}{fg=soleilgrey}

% Itemize symbols
\setbeamertemplate{itemize item}{\tikz[baseline=-0.5ex]\draw[soleilblue, fill=soleilblue,radius=1.1pt] (0,0) circle ;}
\setbeamertemplate{itemize subitem}{\color{soleilgrey}--}
\setbeamertemplate{itemize subsubitem}{\tikz[baseline=-0.5ex]\draw[soleilgrey, fill=soleilgrey,radius=1.1pt] (0,0) circle ;}

% Frame Title
\makeatletter
\setbeamertemplate{frametitle}{
    \ifbeamercolorempty[bg]{frametitle}{}{\nointerlineskip}%
    \@tempdima=\textwidth%
    \advance\@tempdima by\beamer@leftmargin%
    \advance\@tempdima by\beamer@rightmargin%
    \begin{beamercolorbox}[sep=0.19cm,wd=\paperwidth]{frametitle}
        \usebeamerfont{frametitle}%
        \vbox{}\vspace{0.0mm}%
        \fontsize{13.25}{13.25}\selectfont
        \strut\hfill\insertframetitle\hspace{6.35mm}\strut\par%
        {%
            \ifx\insertframesubtitle\@empty%
                \vspace{3mm}
            \else%
                \vskip3pt
                {\usebeamerfont{framesubtitle}\usebeamercolor[fg]{framesubtitle}\hfill\insertframesubtitle\hspace{6.35mm}\strut\par}%
            \fi
        }%
        \vskip-1ex%
    \end{beamercolorbox}%
}

% Footer
\setbeamertemplate{footline}
{%
  \leavevmode%
  \vskip6pt%
  \usebeamercolor[fg]{frametitle}
  \hspace{1.5cm}
  \insertsection
  \setbox0=\hbox{\subsecname\unskip}
        \ifdim\wd0=0pt
        \else%
            \usebeamercolor[fg]{framesubtitle}
            \setbox0=\hbox{\secname\unskip}
                \ifdim\wd0=0pt
                \else
                     ~--~
                \fi
             \insertsubsectionhead%
        \fi%
  \hfill
  \color{soleilblue}
  \fontsize{4.0}{4.0}\selectfont
  \inserttitle%
  \makeatletter%
  \ifthenelse{\equal{\@mainauthor}{}}{
      \ifthenelse{\equal{\@author}{}}{}{%
          \ ~--~\ \insertauthor%
      }
  }{
      \ ~--~\ \insertmainauthor%
  }
  \makeatother%
  \hspace{0.1cm}{\color{soleilyellow}\vrule width 0.6pt}\hspace{0.1cm}%
  \parbox{0.5cm}{\textbf\insertframenumber}\hspace{1.70cm}
  \vskip10.0pt%
  \begin{tikzpicture}[remember picture,overlay]
          \node[xshift=-1.18cm,yshift=0.50cm] at (current page.south east) {\includegraphics[width=1.40cm,trim={4.8cm 0cm 0cm 0cm},clip]{./images/soleil_logo.png}};
  \end{tikzpicture}
}
\makeatother

\beamertemplatenavigationsymbolsempty

% Define Title Page
\makeatletter
\newcommand{\gettikzxy}[3]{%
    \tikz@scan@one@point\pgfutil@firstofone#1\relax
    \edef#2{\the\pgf@x}%
    \edef#3{\the\pgf@y}%
}
\setbeamertemplate{title page}
{
  \begin{adjustwidth}{-1cm}{0cm}  % Shift titlepage to counter act text margin left = 2cm ... 1 is default
    \vskip0pt plus 1filll

      \begin{tikzpicture}[remember picture,overlay]
          \filldraw[fill=white,draw=white] (current page.north west) rectangle (current page.south east);  % hide normal slide background
          \node[xshift=-0.13cm,yshift=-0.124cm,anchor=south west] at (current page.south west) {\includegraphics[width=\paperwidth]{./images/title_bg.jpg}};
          \node[xshift=-1.26cm,yshift=0.73cm] at (current page.south east) {\includegraphics[width=2.40cm,trim={4.8cm 0cm 0cm 0cm},clip]{./images/soleil_logo_white.png}};
          \node[xshift=-0.58cm,yshift=-0.70cm] at (current page.north east) {\includegraphics[width=5.60cm]{./images/title_topright.png}};
          \node[xshift=-1.46cm,yshift=-0.65cm] at (current page.north east) {\includegraphics[width=2.18cm,trim={0cm 0cm 4.8cm 0cm},clip]{./images/soleil_logo.png}};
          \filldraw[line width=0.06mm,fill=white,draw=white] ($ (current page.north west)+ (1.85cm, 10.8cm)$) -- ($ (current page.north west)+ (1.85cm, -10cm)$);
          \node[anchor=south west] (title) at ($( current page.north west) + (2.0cm, -6.8cm)$) {%
                \parbox{13.5cm}{%
                    \parbox{13.5cm}{%
                        \usebeamerfont{title}%
                        \fontsize{\@titlesize}{\@titlesize}\selectfont%
                        \color{white}%
                        \textbf{\inserttitle}%
                    }%
                    \ifthenelse{\equal{\insertsubtitle}{}}{}{%
                        \vspace{1.5mm}\\%
                        \parbox{13.5cm}{%
                            \usebeamerfont{subtitle}%
                            \fontsize{11}{11}\selectfont%
                            \color{white}%
                            \insertsubtitle%
                        }%
                    }%
                }%
          };
          \gettikzxy{(title.north east)}{\ax}{\ay}
          \gettikzxy{(title.south east)}{\bx}{\by}
          \gettikzxy{(current page.north west)}{\pagex}{\pagey}
          \filldraw[rounded corners=0.05cm,fill=soleilyellow,draw=soleilyellow] ($ (\pagex+1.802cm, \ay+1mm)$) rectangle ($ (\pagex + 1.898cm, \by-1mm)$);
          \node[xshift=0.36cm,yshift=0.850cm] at (current page.south west) {\includegraphics[width=0.735cm]{./images/soleil_bottomleft.png}};

          \ifthenelse{\equal{\@showmetadata}{0}}{}{%
                \node[anchor=north west] at ($( current page.north west) + (2.0cm, -7.20cm)$) {%
                    \parbox{11.5cm}{%
                        \setstretch{1.5}%
                        \usebeamerfont{subtitle}\fontsize{7}{7}\selectfont\color{white}%
                        \ifthenelse{\equal{\theevent}{}}{}{%
                            \theevent \\%
                        }%
                        \insertauthor \\%
                        \insertdate%
                    }%
                };
          }
      \end{tikzpicture}
  \end{adjustwidth}
    \setbeamertemplate{footline}{}
}
\makeatother

\newcommand{\sectiontitle}[2][]{
    \begin{frame}[plain]
      \begin{adjustwidth}{-1cm}{0cm}  % Shift titlepage to counter act text margin left = 2cm ... 1 is default
        \vskip0pt plus 1filll
          \begin{tikzpicture}[remember picture,overlay]
              \filldraw[fill=white,draw=white] (current page.north west) rectangle (current page.south east);  % hide normal slide background
              \ifthenelse{\equal{#1}{blue}}{
                  \node[xshift=-0.13cm,yshift=-0.124cm,anchor=south west] at (current page.south west) {\includegraphics[width=\paperwidth]{./images/soleil_section_bg_blue.jpg}};
                  \node[xshift=1.13cm,yshift=-0.68cm] at (current page.north west) {\includegraphics[width=2.38cm,trim={0cm 0cm 4.8cm 0cm},clip]{./images/soleil_logo_white.png}};
                  \node[xshift=-2.50cm,yshift=0.68cm] at (current page.south east) {\includegraphics[width=2.38cm,trim={4.8cm 0cm 0cm 0cm},clip]{./images/soleil_logo_white.png}};
                  \def\@color{white}
              }{
                  \node[xshift=-0.13cm,yshift=-0.124cm,anchor=south west] at (current page.south west) {\includegraphics[width=\paperwidth]{./images/soleil_section_bg.jpg}};
                  \node[xshift=1.13cm,yshift=-0.68cm] at (current page.north west) {\includegraphics[width=2.38cm,trim={0cm 0cm 4.8cm 0cm},clip]{./images/soleil_logo.png}};
                  \node[xshift=-2.50cm,yshift=0.68cm] at (current page.south east) {\includegraphics[width=2.38cm,trim={4.8cm 0cm 0cm 0cm},clip]{./images/soleil_logo.png}};
                  \def\@color{soleilblue}
              }
              \filldraw[line width=0.06mm,fill=\@color,draw=\@color] ($ (current page.north west)+ (14.75cm, 10.8cm)$) -- ($ (current page.north west)+ (14.75cm, -10cm)$);
              \ifthenelse{\equal{#2}{}}{
                \filldraw[rounded corners=0.05cm,fill=soleilyellow,draw=soleilyellow] ($ (current page.north west)+ (14.70cm, -2.72cm)$) rectangle ($ (current page.north west)+ (14.80cm, -3.85cm)$);
                \node[anchor=north east] at ($( current page.north west) + (14.5cm, -2.88cm)$) {\usebeamerfont{title}\fontsize{17}{17}\selectfont\color{\@color}\textbf{\insertsectionhead}};
              }{
                \filldraw[rounded corners=0.05cm,fill=soleilyellow,draw=soleilyellow] ($ (current page.north west)+ (14.70cm, -2.72cm)$) rectangle ($ (current page.north west)+ (14.80cm, -3.85cm)$);
                \node[anchor=north east] at ($( current page.north west) + (14.5cm, -2.70cm)$) {\usebeamerfont{title}\fontsize{17}{17}\selectfont\color{\@color}\textbf{\insertsectionhead}};
                \node[anchor=north east] at ($( current page.north west) + (14.5cm, -3.38cm)$) {\usebeamerfont{title}\fontsize{9.5}{9.5}\selectfont\color{\@color} #2 };
              }
              \node[xshift=0.36cm,yshift=0.850cm] at (current page.south west) {\includegraphics[width=0.735cm]{./images/soleil_bottomleft.png}};
          \end{tikzpicture} 
      \end{adjustwidth}
  \end{frame}
}

\newcommand{\soleiltitle}{
    \begin{frame}[plain]
        \maketitle
    \end{frame}
}


\makeatletter
\newcommand{\globalcolor}[1]{%
  \color{#1}\global\let\default@color\current@color
}
\makeatother

\AtBeginDocument{\globalcolor{soleilgrey}}

