# Soleil2BeamerTemplate

Template for SOLEIL II latex beamer. Uses the new corporate design since April 24

## How to Use

Copy the file ``Soleil2Beamer.cls`` as well as the ``images`` directory next to your tex file.

Then you can follow the ``example.tex`` or follow these steps

In the preamble:

1. Use ``\documentclass{Soleil2Beamer}``
2. Set the title with ``\title{}``. The fontsize can be changed with ``\title[<size>]{}``
3. Optionally: Set a subtitle ``\subtitle{}``
4. Optionally: Set an author with ``\author{}``. To indicate the main author to be used on each slide with ``\author[main author]{list of authors}``. If no main author is given the full list will be shown on each slide
5. Optionally: Set an event name with ``\event{}``
6. Optionally: To show Author, Date and Event on the titleslide use ``\showauthordate`` in the preamble

In the document:

7. Optionally: Set the titlepage with ``\soleiltitle``. This will already create the correct frame
8. Optionally: Create sections. To set a section title page use ``\sectiontitle[<color>]{Optional subtitle}`` where `\<color>`` is either ``blue`` or ``white``
9. Normally write your slides as with any other LaTeX beamer template
